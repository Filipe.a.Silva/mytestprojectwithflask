# MyTestProjectWithFlask

Pipeline Status:

[![pipeline status](https://gitlab.com/Filipe.a.Silva/mytestprojectwithflask/badges/master/pipeline.svg)](https://gitlab.com/Filipe.a.Silva/mytestprojectwithflask/-/commits/master)

## Cheatsheet:

### Variables:
- $CI_REGISTRY_IMAGE -> Repository's Image Registry
- $CI_PROJECT_ID -> Repository's name/path

### Commands:
- docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY -> Login to docker in CLI
- 'curl --header "JOB-TOKEN: \$CI_JOB_TOKEN" --upload-file FILE "${CI_API_V4_URL}/projects/\${CI_PROJECT_ID}/packages/generic/PACKAGE AND VERSION/FILE"' -> Command to send package to repository's package 

### Utils:

- image: $CI_REGISTRY_IMAGE/IMAGE -> Use a image from the repository 
- To run a job only when there are changes use "only:\n\t changes:\n\t\t - Files"
-  To push an image into a repository use_ (docker login, docker build -t NAME DOCKERFILELOCATION, docker push NAME)
