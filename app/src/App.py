#!/usr/bin/env python
# encoding: utf-8
import json
from flask import Flask,redirect, jsonify,request


server = Flask(__name__)
@server.route('/', methods=['GET'])
def getAllInfo():
    data = json.load(open("mockData.json"))
    return jsonify(data)
@server.route('/', methods=['POST'])
def addInfo():
    data = json.load(open("mockData.json"))
    data.append(json.loads(request.data))
    open('mockData.json', 'w').write(json.dumps(data, indent=2))
    #object.append({'name':'AddedName','email':'addedMock@email.com'})
    return redirect('/')
    
if __name__ == "__main__":
    server.run(host='0.0.0.0', port=5000)