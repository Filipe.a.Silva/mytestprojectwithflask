import requests
import unittest
import json
import sys


 
def test_get():
    response = requests.get('http://127.0.0.1:5000/')
    assert response.status_code == 200
    assert len(response.json())>0

def test_post():
    response = requests.post('http://127.0.0.1:5000/', data="{\"name\":\"Mock\",\"email\":\"Email\"}")
    assert response.status_code == 200
    assert len(response.json())>0
